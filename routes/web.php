<?php

use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('start');
});

Auth::routes();

Route::get('/prize', 'PrizeController@prize')->name('prize');
Route::post('/action', 'PrizeController@action')->name('action');

// Reset
Route::get('/reset', function(){
    auth()->user()->update([
        'bonus' => 0
    ]);
    // Truncate data
    DB::table('prizes')->truncate();
    DB::table('withdraws')->truncate();
    DB::table('deliveries')->truncate();

    return redirect('/');
});
