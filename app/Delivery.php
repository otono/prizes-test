<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    protected $table = 'deliveries';
    protected $guarded = ['id'];

    // User
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    // Thing
    public function thing()
    {
        return $this->belongsTo('App\Thing');
    }
}
