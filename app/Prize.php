<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Events\PrizeCreated;

class Prize extends Model
{
    protected $guarded = ['id'];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => PrizeCreated::class,
    ];

    // User
    public function user(){
        return $this->belongsTo('App\User');
    }

    // Thing
    public function thing(){
        return $this->belongsTo('App\Thing');
    }

    // Action
    public function action($type)
    {
        // Withdraw
        switch($type)
        {
            // Refuse
            case 'refuse':
                $this->update([
                    'refused' => true,
                ]);
                break;

            // Withdraw
            case 'withdraw':
                if($this->prize_type == 'money'){
                    $this->action_withdraw();
                }
                break;

            // Convert
            case 'convert':
                if($this->prize_type == 'money'){
                    $this->convert();
                }
                break;

            // Transfer to bonus account
            case 'transfer':
                if($this->prize_type == 'bonus'){
                    $this->transfer_bonus();
                }
                break;
        }

        // Update processed status
        $this->update(['processed' => true, 'action_type' => $type]);
    }

    // Withdraw
    public function action_withdraw()
    {
        \App\Withdraw::create([
            'user_id' => $this->user_id,
            'amount' => $this->amount
        ]);
    }

    // Convert
    public function convert()
    {
        $this->user->update([
            'bonus' => $this->user->bonus + $this->amount * get_setting('convert_rate')
        ]);
    }

    // Transfer to bonus account
    public function transfer_bonus()
    {
        $this->user->update([
            'bonus' => $this->amount
        ]);
    }

    // Delivery
    public function delivery()
    {
        return $this->hasOne('App\Delivery');
    }

    public function action_delivery($address)
    {
        \App\Delivery::create([
            'user_id' => $this->user_id,
            'address' => $address,
            'thing_id' => $this->thing_id,
            'prize_id' => $this->id
        ]);

        $this->update(['processed' => true, 'action_type' => 'delivery']);
    }
}
