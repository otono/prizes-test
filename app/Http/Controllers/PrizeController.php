<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PrizeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        // Check if user has prize
        $this->middleware(function($request, $next){
            if(auth()->user()->prize !== null){
                return redirect('/');
            }
            return $next($request);
        })->only('prize');

        // Check prize for action
        $this->middleware(function($request, $next){
            // If user has no prize or prize refused
            if(auth()->user()->prize == null || auth()->user()->prize->refused){
                return redirect('/');
            }
            return $next($request);
        })->only('action');
    }

    /**
     * Get prize
     *
     * @return \Illuminate\Http\Response
     */
    public function prize()
    {
        // Prize types
        $prize_types = ['bonus'];

        // Available money bank
        $money_prize_bank = get_setting('money_prize_bank');
        if($money_prize_bank > 0){
            $prize_types[] = 'money';
            // Max money prize amount
            $max_money_prize_setting = get_setting('max_money_prize');
            $max_money_amount = ($money_prize_bank >= $max_money_prize_setting)
                ? $max_money_prize_setting
                : $money_prize_bank;
        }

        // Available things bank
        $available_things = \App\Thing::where('quantity', '<>', 0)->get();
        if($available_things->count()){
            $prize_types[] = 'thing';
        }

        // Random prize type
        $prize_data['prize_type'] = $prize_types[rand(0, count($prize_types) - 1)];

        switch($prize_data['prize_type']){
            // Money prize
            case 'money':
                // Random money amount
                $prize_data['amount'] = rand(
                    get_setting('min_money_prize'),
                    $max_money_amount
                );

                break;

            // Bonus prize
            case 'bonus':
                // Random bonus amount
                $prize_data['amount'] = rand(
                    get_setting('min_bonus_prize'),
                    get_setting('max_bonus_prize')
                );

                break;

            // Thing prize
            case 'thing':
                $prize_data['thing_id'] = $available_things->random()->id;

                break;
        }

        // Save user's prize
        $prize = new \App\Prize($prize_data);
        auth()->user()->prize()->save($prize);
        
        return redirect('/');
    }

    /**
     * Prize action
     */
    public function action(Request $request)
    {
        // Delivery request
        if($request->action == 'delivery'){
            auth()->user()->prize->action_delivery($request->address);
        // Other actions
        } else {
            auth()->user()->prize->action($request->action);
        }

        return redirect('/');
    }
}
