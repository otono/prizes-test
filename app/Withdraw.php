<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{
    protected $guarded = ['id'];

    // User
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    // Send to bank API
    public function send_to_bank()
    {
        // Bank API endpoint
        $endpoint = '';
        $client = new \GuzzleHttp\Client();

        try {
            $res = $client->request('POST', $endpoint, [
                'token' => '__TOKEN__',
                'amount' => $this->amount,
                'client_id' => $this->user->id,
                'comment' => 'Начисление призовых средств',
                // ...
            ]);
        } catch(\GuzzleHttp\Exception\RequestException $e){
            // Error exception
            dd($e->getMessage());
        }
    }
}
