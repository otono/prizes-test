<?php

namespace App\Listeners;

use App\Events\PrizeCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;

class UpdateSettings
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PrizeCreated  $event
     * @return void
     */
    public function handle(PrizeCreated $event)
    {
        switch($event->prize->prize_type){
            // Update money prize bank
            case 'money':
                DB::table('settings')->where('name', 'money_prize_bank')
                    ->update([
                        'value' => get_setting('money_prize_bank') - $event->prize->amount
                    ]);

                break;

            // Update thing quantity
            case 'thing':
                $event->prize->thing->update([
                    'quantity' => $event->prize->thing->quantity - 1
                ]);

                break;
        }
    }
}
