<?php

use Illuminate\Support\Facades\DB;

/**
 * Prize bank
 */
if (! function_exists('get_setting')) {
    function get_setting($type){
        return DB::table('settings')
            ->where('name', $type)
            ->first()
            ->value;
    }
}