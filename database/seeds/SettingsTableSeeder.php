<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Money prize bank
        DB::table('settings')->insert([
            'name' => 'money_prize_bank',
            'value' => 1000000,
        ]);

        // Min money prize
        DB::table('settings')->insert([
            'name' => 'min_money_prize',
            'value' => 100,
        ]);

        // Max money prize
        DB::table('settings')->insert([
            'name' => 'max_money_prize',
            'value' => 10000,
        ]);

        // Min bonus prize
        DB::table('settings')->insert([
            'name' => 'min_bonus_prize',
            'value' => 1000,
        ]);

        // Max bonus prize
        DB::table('settings')->insert([
            'name' => 'max_bonus_prize',
            'value' => 10000,
        ]);

        // Convert rate
        DB::table('settings')->insert([
            'name' => 'convert_rate',
            'value' => 50,
        ]);
    }
}
