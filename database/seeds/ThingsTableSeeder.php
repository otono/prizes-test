<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ThingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $things_list = file(base_path() . '/things', FILE_IGNORE_NEW_LINES);

        foreach($things_list as $thing){
            DB::table('things')->insert([
                'name' => $thing,
                'quantity' => rand(1, 10),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
