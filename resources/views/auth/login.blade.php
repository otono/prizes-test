@extends('layouts.app')

@section('content')
    <h1 class="title">Авторизация</h1>
    <div class="columns">
        <div class="column is-half">
            <form method="POST" action="{{ route('login') }}">
                @csrf

                <div class="field">
                    <label class="label">E-mail</label>
                    <div class="control">
                        <input name="email" class="input {{ $errors->has('email') ? ' is-danger' : '' }}" type="email" value="{{ old('email') }}" autofocus>
                    </div>
                    @if ($errors->has('email'))
                        <p class="help is-danger">{{ $errors->first('email') }}</p>
                    @endif
                </div>

                <div class="field">
                    <label class="label">Пароль</label>
                    <div class="control">
                        <input name="password" class="input {{ $errors->has('password') ? ' is-danger' : '' }}" type="password">
                    </div>
                    @if ($errors->has('password'))
                        <p class="help is-danger">{{ $errors->first('password') }}</p>
                    @endif
                </div>

                <div class="field">
                    <div class="control">
                        <label class="checkbox">
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            Запомнить меня
                        </label>
                    </div>
                </div>

                <div class="field">
                    <div class="control">
                        <button type="submit" class="button is-link">Войти</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
