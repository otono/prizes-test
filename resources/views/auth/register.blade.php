@extends('layouts.app')

@section('content')
    <h1 class="title">Регистрация</h1>
    <div class="columns">
        <div class="column is-half">
            <form method="POST" action="{{ route('register') }}">
                @csrf
                <div class="field">
                    <label class="label">Имя</label>
                    <div class="control">
                        <input name="name" class="input {{ $errors->has('name') ? ' is-danger' : '' }}" type="text" value="{{ old('name') }}" autofocus>
                    </div>
                    @if ($errors->has('name'))
                        <p class="help is-danger">{{ $errors->first('name') }}</p>
                    @endif
                </div>
                <div class="field">
                    <label class="label">E-mail</label>
                    <div class="control">
                        <input name="email" class="input {{ $errors->has('email') ? ' is-danger' : '' }}" type="email" value="{{ old('email') }}">
                    </div>
                    @if ($errors->has('email'))
                        <p class="help is-danger">{{ $errors->first('email') }}</p>
                    @endif
                </div>
                <div class="field">
                    <label class="label">Пароль</label>
                    <div class="control">
                        <input name="password" class="input {{ $errors->has('password') ? ' is-danger' : '' }}" type="password">
                    </div>
                    @if ($errors->has('password'))
                        <p class="help is-danger">{{ $errors->first('password') }}</p>
                    @endif
                </div>
                <div class="field">
                    <label class="label">Подтвердите пароль</label>
                    <div class="control">
                        <input name="password_confirmation" class="input" type="password">
                    </div>
                </div>
                <div class="field">
                    <div class="control">
                        <button type="submit" class="button is-link">Зарегистрироваться</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
