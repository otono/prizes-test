@extends('layouts.app')

@section('content')
    @guest
        <p class="is-size-5">Для участия в розыгрыше необходимо авторизоваться.</p>
        <br>
        <div class="field is-grouped">
            <div class="control">
                <a href="/login" class="button is-light">Войти</a>
            </div>
            <div class="control">
                <a href="/register" class="button is-link">Зарегистрироваться</a>
            </div>
        </div>
    @else
        @if(Auth::user()->hasePrize())
            <h1 class="title">Поздравляем!</h1>
            <p class="is-size-5">
                Вы выиграли
                <b>{{ trans('app.'.auth()->user()->prize->prize_type) }}
                    @if(auth()->user()->prize->prize_type == 'money')
                        ${{ auth()->user()->prize->amount }}
                    @elseif(auth()->user()->prize->prize_type == 'bonus')
                        {{ auth()->user()->prize->amount }}
                    @else 
                        {{ auth()->user()->prize->thing->name }}
                    @endif
                </b>
            </p>
            <br>
            @if(auth()->user()->prize->refused)
                <p class="is-size-5 has-text-danger">Вы отказались от приза.</p>
            @else
                <form action="{{ route('action') }}" method="POST">
                    @csrf
                    @if(auth()->user()->prize->prize_type == 'money')
                        @if(auth()->user()->prize->processed)
                            @if(auth()->user()->prize->action_type == 'withdraw')
                                <p class="is-size-5 has-text-success">Отправлен запрос на перевод денежных средств на счёт в банке.</p>
                            @elseif(auth()->user()->prize->action_type == 'convert')
                                <p class="is-size-5 has-text-success">Призовые деньги конвертированы в бонусные баллы.</p> 
                            @endif
                            <br>
                        @else
                            <div class="field">                
                                <p>
                                    <label class="radio">
                                        <input type="radio" name="action" value="withdraw" checked>
                                        Перевести на счет в банке
                                    </label>
                                </p>
                                <p>
                                    <label class="radio">
                                        <input type="radio" name="action" value="convert">
                                        Конвертировать в баллы
                                    </label>
                                </p>
                                <p>
                                    <label class="radio">
                                        <input type="radio" name="action" value="refuse">
                                        Отказаться от приза
                                    </label>
                                </p>
                            </div>
                        @endif
                    @endif

                    @if(auth()->user()->prize->prize_type == 'bonus')
                        @if(auth()->user()->prize->processed)
                            <p class="is-size-5 has-text-success">Баллы зачеслены на ваш бонусный счет.</p>
                            <br>
                        @else
                            <div class="field">                
                                <p>
                                    <label class="radio">
                                        <input type="radio" name="action" value="transfer" checked>
                                        Зачислить на бонусный счет
                                    </label>
                                </p>
                                <p>
                                    <label class="radio">
                                        <input type="radio" name="action" value="refuse">
                                        Отказаться от приза
                                    </label>
                                </p>
                            </div>
                        @endif
                    @endif

                    @if(auth()->user()->prize->prize_type == 'thing')
                        @if(auth()->user()->prize->processed)
                            <p class="is-size-5 has-text-success">Приз будет отправлен по адресу:</p>
                            <p class="is-size-5">{{ auth()->user()->prize->delivery->address }}.</p>
                            <br>
                        @else
                            <div class="field">                
                                <p>
                                    <div class="field">
                                        <label class="radio">
                                            <input type="radio" name="action" value="delivery" checked>
                                            Отправить по почте
                                        </label>
                                    </div>
                                    <div class="field">
                                        <div class="control">
                                            <textarea class="textarea" name="address" placeholder="Укажите почтовый адрес для отправки"></textarea>
                                        </div>
                                    </div>
                                </p>
                                <p>
                                    <label class="radio">
                                        <input type="radio" name="action" value="refuse">
                                        Отказаться от приза
                                    </label>
                                </p>
                            </div>
                        @endif
                    @endif

                    @if(!auth()->user()->prize->processed)
                        <div class="field">
                            <p class="control">
                                <button type="submit" class="button is-info">
                                Подтвердить
                                </button>
                            </p>
                        </div>
                    @endif
                </form>
            @endif
        @else
            <p class="is-size-5">
                {{ Auth::user()->name }}, нажмите на кнопку для участия в розыгрыше!
            </p>
            <br>
            <a href="{{ route('prize') }}" class="button is-success is-large">Получить приз!</a>
        @endif
    @endguest
@endsection