<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Розыгрыш призов</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css">
    </head>
    <body>
        <section class="hero is-danger is-bold">
            <div class="hero-body">
                <div class="container">
                    <div class="columns">
                        <div class="is-four-fifths">
                            <h1 class="title">
                                <a href="/">Розыгрыш призов!</a>
                            </h1>
                            <h2 class="subtitle">
                                Выигрывайте деньги, бонусные баллы, а так же призы от наших спонсоров!
                            </h2>
                        </div>
                        <div class="column has-text-right">
                            @if(Auth::user())
                                <b>{{ Auth::user()->name }}</b> | 
                                Бонусы: <b>{{ Auth::user()->bonus }}</b>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    Выйти
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section">
            <div class="container">
                @yield('content')
            </div>
        </section>
        @if(Auth::user() && Auth::user()->hasePrize())
        <footer class="footer">
            <div class="content has-text-centered">
                <p>ТЕСТ:</p>
                <a href="/reset" class="button is-danger">
                    Сбросить данные
                </a>
            </div>
        </footer>
        @endif
    </body>
</html>