## Тестовое задание

Framework: Laravel 5.7

```sh
$ composer update
$ php artisan migrate
$ php artisan db:seed
$ php artisan serve
```

[http://test.kuzmani.com](http://test.kuzmani.com)